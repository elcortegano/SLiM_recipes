# SLiM recipes

A collection of recipes for the [SLiM](https://messerlab.org/slim/) software, that I have used in research.

## Overview

Below there is a brief description of each recipe. If you use one of these or parts of it, please ccite the related publication:

- `MA_C3H.slim`: Replicates the MA experiment used in López-Cortegano et al. (in prep) to estimate variation in the count of mutations among MA lines of the C3H strain.

## References

 - López-Cortegano E, et al. Variation in the spectrum of new mutations among inbred strains of mice. *In prep.*
